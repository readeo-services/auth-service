import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Metadata } from 'grpc'

@Injectable()
export class ApikeyGuard implements CanActivate{
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const ctx = context.switchToRpc().getContext() as Metadata
    const map = ctx.getMap()

    return map.apikey && map.apikey == map.apisecret
  }
}
