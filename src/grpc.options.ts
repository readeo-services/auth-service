import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

export const userMicroserviceOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: 'user',
    url: 'readeo-service-user:5000',
    protoPath: join(__dirname, '../src/proto/user.proto'),
  },
};

export const authMicroserviceOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: 'auth',
    url: '0.0.0.0:5000',
    protoPath: join(__dirname, '../src/proto/auth.proto'),
  },
};
