import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { authMicroserviceOptions } from './grpc.options';
import { MicroserviceOptions } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    authMicroserviceOptions
  );
  app.listen(() => console.log('Microservice is listening'));
}
bootstrap();
