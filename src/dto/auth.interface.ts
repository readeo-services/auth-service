export interface IAuthUserCredentials {
  email: string
  password: string
}

export interface IAuthAccessToken {
  accessToken: string
}

export interface IAuthUserData {
  userId: string
}
