import { Observable } from 'rxjs';

export interface IUserService {
  verifyUser(data: { email: string, password: string }): Observable<any>;
}

export interface IUserData {
  _id: string
  email: string
}
