import { Controller, Logger } from '@nestjs/common';
import { AppService } from './app.service';
import { GrpcMethod } from '@nestjs/microservices';
import { IAuthAccessToken, IAuthUserData, IAuthUserCredentials } from './dto/auth.interface';

@Controller()
export class AppController {
  constructor(private readonly authService: AppService) { }

  @GrpcMethod("AuthService")
  async authenticateUser(params: IAuthUserCredentials): Promise<IAuthAccessToken> {
    const user = await this.authService.validateUser(params.email, params.password);

    return this.authService.login(user);
  }

  @GrpcMethod("AuthService")
  async validateToken(params: IAuthAccessToken): Promise<IAuthUserData> {
    try {
      const validity = await this.authService.validateToken(params.accessToken);
      return { userId: validity.sub };
    } catch (e) {
      Logger.log(e);
      return { userId: null };
    }
  }
}
