import { Injectable, Logger, RequestTimeoutException, OnModuleInit } from '@nestjs/common';
import { ClientGrpc, Client } from '@nestjs/microservices';
import { JwtService } from '@nestjs/jwt'
import { timeout, catchError } from 'rxjs/operators';
import { TimeoutError, throwError } from 'rxjs';
import { IUserService, IUserData } from './dto/user.interface';
import { userMicroserviceOptions } from './grpc.options';
import { IAuthAccessToken } from './dto/auth.interface';

@Injectable()
export class AppService implements OnModuleInit {
  private userService: IUserService;

  @Client(userMicroserviceOptions)
  private client: ClientGrpc

  constructor(
    private jwtService: JwtService,
  ) { }

  onModuleInit(): void {
    this.userService = this.client.getService<IUserService>('UsersController');
  }

  async validateUser(email: string, password: string): Promise<IUserData> {
    try {
      const user = await this.userService.verifyUser({ email, password })
        .pipe(
          timeout(5000),
          catchError(err => {
            if (err instanceof TimeoutError) {
              return throwError(new RequestTimeoutException());
            }
            return throwError(err);
          }),
        )
        .toPromise<IUserData>();

      return user;
    } catch (e) {
      Logger.log(e);
      throw e;
    }
  }

  async login(user: IUserData): Promise<IAuthAccessToken> {
    const payload = { email: user.email, sub: user._id };

    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async validateToken(jwt: string): Promise<any> {
    return this.jwtService.verify(jwt);
  }
}
